How to create the MVC Music Store application, a sample application that sells music albums online, and that implements site administration, user sign-in, and shopping cart functionality.



Part 1: Overview and File->New Project
Part 2: Controllers
Part 3: Views and ViewModels
Part 4: Models and Data Access
Part 5: Edit Forms and Templating
Part 6: Using Data Annotations for Model Validation
Part 7: Membership and Authorization
Part 8: Shopping Cart with Ajax Updates
Part 9: Registration and Checkout
Part 10: Final Updates to Navigation and Site Design, Conclusion




[https://docs.microsoft.com/en-us/aspnet/mvc/overview/older-versions/mvc-music-store/]
