﻿using MusicStoreMvc.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicStoreMvc.Web.ViewModels
{
    public class GenreAlbumView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Album> Albums { get; set; }
    }
}