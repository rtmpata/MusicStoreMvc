﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MusicStoreMvc.Web.Startup))]
namespace MusicStoreMvc.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
