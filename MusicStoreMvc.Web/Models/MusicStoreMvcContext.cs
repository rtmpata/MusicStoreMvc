﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MusicStoreMvc.Web.Models
{
    public class MusicStoreMvcContext : IdentityDbContext<ApplicationUser>
    {
        public MusicStoreMvcContext()
            : base("MusicStoreMvc", throwIfV1Schema: false)
        {
        }

        public DbSet<Album> Albums { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // customising the table name in the Database
            modelBuilder.Entity<IdentityUser>().ToTable("ApplicationUser", "dbo");
            modelBuilder.Entity<IdentityRole>().ToTable("ApplicationRole", "dbo");
            modelBuilder.Entity<IdentityUserRole>().ToTable("ApplicationUserRole", "dbo");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("ApplicationUserClaim", "dbo");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("ApplicationUserLogin", "dbo");

            base.OnModelCreating(modelBuilder);
        }



        public static MusicStoreMvcContext Create()
        {
            return new MusicStoreMvcContext();
        }

        //public System.Data.Entity.DbSet<MusicStoreMvc.Web.ViewModels.PaymentAndAddressViewModel> PaymentAndAddressViewModels { get; set; }

        //public System.Data.Entity.DbSet<MusicStoreMvc.Web.Models.Genre> Genres { get; set; }
    }
}