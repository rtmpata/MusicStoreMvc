﻿using MusicStoreMvc.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;

namespace MusicStoreMvc.Web.Controllers
{
    public class StoreController : Controller
    {

        private MusicStoreMvcContext db = new MusicStoreMvcContext();
        // GET: Store
        public ActionResult Index()
        {
            var genre = db.Genres.Distinct().ToList();
            return View(genre);
        }

        //GET: Store/Browse
        public ActionResult Browse(string genre, int? page)
        {
            var genreModel = db.Genres.Include("Albums")
                .Single(g => g.Name == genre);
            ////var genreModel = db.Genres.Where(g => g.Name == genre)
            ////                           .Select(g => new GenreAlbumView
            ////                           {
            ////                               Id = g.GenreId,
            ////                               Name = g.Name,
            ////                               Albums = g.Albums
            ////                           }).SingleOrDefault();

            //IQueryable<Album> genreAlbums = from g in db.Albums
            //                  where (g.Genre.Name == genre)
            //                  select g;

            //int pageSize = 3;
            //int pageNumber = (page ?? 1);
            //return View(genreAlbums.ToPagedList(pageNumber, pageSize));

            return View(genreModel);
        }

        // GET : Store/Detail
        public ActionResult Detail(int id)
        {
            var album = db.Albums.Include("Artist").Include("Genre").FirstOrDefault(i => i.AlbumId == id);
            return View(album);
        }
       
        public ActionResult BrowseAlbum(string genre, string searchString)
        {
            //IQueryable<Album> albumModel = db.Albums.Where(g => g.Genre.Name == "Pop");
            IQueryable<Album> albums = db.Albums;
            if (!String.IsNullOrEmpty(genre))
            {
                albums = db.Albums.Where(a => a.Genre.Name.ToUpper().Equals(genre.ToUpper()));
            }
            else
            {
                if (!String.IsNullOrEmpty(searchString))
                {
                    albums = db.Albums.Where(a => a.Title.ToUpper().Contains(searchString.ToUpper())
                                || a.Genre.Name.ToUpper().Equals(genre.ToUpper())
                                || a.Artist.Name.ToUpper().Contains(searchString.ToUpper()));
                }

            }

            var genres = db.Genres.ToList().Distinct();
            List<SelectListItem> genreList = new List<SelectListItem>();
            foreach(var item in genres)
            {
                genreList.Add(new SelectListItem { Text = item.Name, Value = item.Name });
            }
            ViewBag.Genre = genreList;
            return View(albums.ToList());
        }

        //
        // GET: /Store/GenreMenu
        [ChildActionOnly]
        public ActionResult GenreMenu()
        {
            var genres = db.Genres.ToList();
            return PartialView(genres);
        }
    }
}