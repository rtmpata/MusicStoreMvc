﻿using MusicStoreMvc.Web.BAL;
using MusicStoreMvc.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using MusicStoreMvc.Web.ViewModels;

namespace MusicStoreMvc.Web.Controllers
{
    //[Authorize]
    public class CheckoutController : Controller
    {
        private MusicStoreMvcContext db = new MusicStoreMvcContext();
        const string PromoCode = "FREE";

        //
        // GET: /Checkout/AddressAndPayment
        public ActionResult AddressAndPayment()
        {
            var cart = ShoppingCart.GetCart(this.HttpContext);
            var total = new PaymentAndAddressViewModel
            {
                Total = cart.GetTotal()
            };
            return View();
        }

        //
        // POST: /Checkout/AddressAndPayment
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult AddressAndPayment(FormCollection values)
        public ActionResult AddressAndPayment([Bind(Include = "FirstName,LastName,Address,City,State,Phone,PostalCode,Email,Country")] PaymentAndAddressViewModel address)
        {
            var order = new Order();
            TryUpdateModel(order);

            try
            {
                //if (string.Equals(address["PromoCode"], PromoCode,
                //    StringComparison.OrdinalIgnoreCase) == false)
                //{
                //    return View(order);
                //}
                //else
                //{
                    order.Username = User.Identity.Name;
                    order.OrderDate = DateTime.Now;

                    //Save Order
                    db.Orders.Add(order);
                    db.SaveChanges();
                    //Process the order
                    var cart = ShoppingCart.GetCart(this.HttpContext);
                    cart.CreateOrder(order);

                    return RedirectToAction("Complete",
                        new { id = order.OrderId });
                //}
            }
            catch
            {
                //Invalid - redisplay with errors
                return View(order);
            }
        }


        // GET: /Checkout/Complete
        public ActionResult Complete(int id)
        {
            // Validate customer owns this order
            bool isValid = db.Orders.Any(
                o => o.OrderId == id &&
                o.Username == User.Identity.Name);

            if (isValid)
            {
                return View(id);
            }
            else
            {
                return View("Error");
            }
        }


    }
}